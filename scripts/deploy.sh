#!/bin/bash

application=$1

cd /opt/stc/$application
git fetch origin 
git reset --hard origin/master

git clean -dxf

cd /opt/stc
docker-compose up -d --build --no-deps $application

