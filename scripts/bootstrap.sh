#!/bin/bash

# Inspired by the following guides:
#   - https://www.thefanclub.co.za/how-to/how-secure-ubuntu-1604-lts-server-part-1-basics
#   - https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04

echo 'Hold on to your hats, we are bootsrapping!'

# https://stackoverflow.com/questions/23369917/cleaner-way-to-write-multiple-sed-commands
echo 'Hardening SSH config...'
sed -i.bak -r  \
    -e 's/^(\s*#\s*)?(PermitRootLogin) yes/\2 no/' \
    -e 's/^(\s*#\s*)?(PermitEmptyPasswords) yes/\2 no/' \
    -e 's/^(\s*#\s*)?(PasswordAuthentication) yes/\2 no/' \
    -e 's/^(\s*#\s*)?(X11Forwarding) yes/\2 no/' \
    -e 's/^(\s*#\s*)?(DebianBanner) yes/\2 no/' \
    /etc/ssh/sshd_config

echo 'Installing Firewall (ufw)...'
apt-get install -y ufw

ufw allows ssh
ufw allows http
ufw allows https
ufw enable
ufw status verbose

apt-get install unattended-upgrades
